/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  env: {
    API_URL: process.env.API_URL,
    GOOGLE_ANALYTICS: process.env.GOOGLE_ANALYTICS
  }
}

module.exports = nextConfig
