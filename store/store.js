import thunk from "redux-thunk";
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import subjectSlice from "./features/subject/slice";

const reducers = combineReducers({
  subject: subjectSlice,
});

export const store = configureStore({
  reducer: reducers,
});
