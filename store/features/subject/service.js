import axios from "axios";

export const fetchSubjects = async (grade) => {
  const API_URL = process.env.API_URL;
  const { data } = await axios.get(`${API_URL}/subjects?grade=${grade}`);
  return data;
};
