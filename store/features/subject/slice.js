import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import * as api from "./service";

export const fetchSubjects = createAsyncThunk(
  "subjects/fetch",
  async (grade, thunkAPI) => {
    try {
      return await api.fetchSubjects(grade);
    } catch (error) {
      const message =
        (error.response &&
          error.response.data &&
          error.response.data.message) ||
        error.message ||
        error.toString();
      return thunkAPI.rejectWithValue(message);
    }
  }
);

const initialState = {
  isLoading: true,
  isError: false,
  errorMessage: "",
  subjects: [],
  isProcessing: false,
  grade: "12th",
};

export const subjectSlice = createSlice({
  name: "subject",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(fetchSubjects.pending, (state, action) => {
        state.isLoading = true;
        state.subjects = [];
      })
      .addCase(fetchSubjects.fulfilled, (state, action) => {
        state.subjects = action.payload;
        state.isLoading = false;
      })
      .addCase(fetchSubjects.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.errorMessage = action.payload;
      });
  },
});

export default subjectSlice.reducer;
