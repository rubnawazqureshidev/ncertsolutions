import Head from "next/head";
import Link from "next/link";
import Card from "../components/Card";
import Layout from "../components/Layout";

export default function Home({ subjects, grade }) {
  return (
    <>
      <Head>
        <title>
          NCERT Solutions for CBSE Class 1 to 12, Free PDF 2021 - 2022
        </title>
        <meta description="NCERT Solutions for Class 1 to 12 for all subjects that are prepared by our subject matter experts are given here. Students studying from NCERT Textbooks can refer to this page to find solutions for all subjects. " />
      </Head>
      <Layout>
        <p>
          NCERT Solutions for Class 1 to 12 for all subjects with exemplar that
          are prepared by our subject matter experts are given here. Students
          studying from NCERT Textbooks can refer to this page to find solutions
          for all subjects. These are accurate and developed as per the latest
          syllabus.
        </p>
        <h2>Subjects</h2>
        {subjects?.map((subject) => (
          <span key={subject}>
            <Card title={subject} path={`/subjects/${grade}/${subject}`} />
          </span>
        ))}
      </Layout>
    </>
  );
}

// This gets called on every request
export async function getServerSideProps(ctx) {
  const grade = "12th";
  const res = await fetch(`${process.env.API_URL}/subjects?grade=${grade}`);
  const subjects = await res.json();

  // Pass data to the page via props
  return { props: { subjects, grade } };
}
