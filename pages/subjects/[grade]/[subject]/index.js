import Head from "next/head";
import Layout from "../../../../components/Layout";
import Card from "../../../../components/Card";

const Subjects = ({ chapters, grade, subject }) => {
  return (
    <>
      <Head>
        <title>Ncertsolutions</title>
      </Head>
      <Layout>
        <h2>Chapters</h2>
        {chapters?.map((chapter) => (
          <span key={chapter.chapter_name}>
            <Card
              title={chapter.chapter_name}
              path={`/subjects/${grade}/${subject}/${chapter.chapter_name}`}
            />
          </span>
        ))}
      </Layout>
    </>
  );
};

// This gets called on every request
export async function getServerSideProps(ctx) {
  const grade = ctx.params.grade;
  const subject = ctx.params.subject;
  const endpoint = `${process.env.API_URL}/chapters?grade=${grade}&subject=${subject}`;
  const res = await fetch(endpoint);
  const chapters = await res.json();

  // Pass data to the page via props
  return { props: { chapters, grade, subject } };
}

export default Subjects;
