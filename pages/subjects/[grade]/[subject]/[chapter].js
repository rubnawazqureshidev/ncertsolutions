import Head from "next/head";
import Layout from "../../../../components/Layout";
import { Viewer } from "@react-pdf-viewer/core";
import { defaultLayoutPlugin } from "@react-pdf-viewer/default-layout";
import "@react-pdf-viewer/core/lib/styles/index.css";
import "@react-pdf-viewer/default-layout/lib/styles/index.css";
import { Worker } from "@react-pdf-viewer/core";
import styles from "./chapter.module.scss";

const Chapter = ({ data }) => {
  const defaultLayoutPluginInstance = defaultLayoutPlugin();

  return (
    <>
      <Head>
        <title>Ncertsolutions</title>
      </Head>
      <Layout>
        <p>
          <small>Class: {data?.[0]?.["class"]}</small>
        </p>
        <h2>{data[0]?.["chapter_name"]}</h2>
        <div className={styles.pdf_container}>
          <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.14.305/build/pdf.worker.min.js">
            <Viewer
              fileUrl={data[0]?.["pdf_link"]}
              plugins={[defaultLayoutPluginInstance]}
            />
          </Worker>
        </div>
      </Layout>
    </>
  );
};

// This gets called on every request
export async function getServerSideProps(ctx) {
  const grade = ctx.params.grade;
  const subject = ctx.params.subject;
  const chapter = ctx.params.chapter;

  const endpoint = `${process.env.API_URL}/chapter?grade=${grade}&subject=${subject}&chapter_name=${chapter}`;

  const res = await fetch(endpoint);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  // Pass data to the page via props
  return { props: { data } };
}

export default Chapter;
