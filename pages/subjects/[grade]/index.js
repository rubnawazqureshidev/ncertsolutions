import Head from "next/head";
import Layout from "../../../components/Layout";
import Card from "../../../components/Card";

const Subjects = ({ subjects, grade }) => {
  return (
    <>
      <Head>
        <title>Ncertsolutions</title>
      </Head>
      <Layout>
        <h2>Subjects</h2>
        {subjects?.map((subject) => (
          <span key={subject}>
            <Card title={subject} path={`/subjects/${grade}/${subject}`} />
          </span>
        ))}
      </Layout>
    </>
  );
};

// This gets called on every request
export async function getServerSideProps(ctx) {
  const grade = ctx.params.grade;
  const res = await fetch(`${process.env.API_URL}/subjects?grade=${grade}`);
  const subjects = await res.json();

  // Pass data to the page via props
  return { props: { subjects, grade } };
}

export default Subjects;
