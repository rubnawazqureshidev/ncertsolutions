import styles from "./card.module.scss";
import Link from "next/link";

const Card = ({ title, path }) => {
  return (
    <>
      <div className={styles.card}>
        <li key={path}>
          <Link href={path}>
            <a>{title}</a>
          </Link>
        </li>
      </div>
    </>
  );
};

export default Card;
