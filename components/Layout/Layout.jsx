import Sidebar from "../Sidebar";
import styles from "./Layout.module.scss";

const Layout = ({ children }) => (
  <>
    <div className={styles.header}>Ncertsolutions</div>
    <div className={styles.main}>
      <Sidebar />
      <div className={styles.contentarea}>{children}</div>
    </div>
  </>
);

export default Layout;
