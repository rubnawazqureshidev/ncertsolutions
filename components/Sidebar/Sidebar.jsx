import styles from "./Sidebar.module.scss";
import Link from "next/link";

const Sidebar = () => {
  const items = [
    "12th",
    "11th",
    "10th",
    "9th",
    "8th",
    "7th",
    "6th",
    "5th",
    "4th",
    "3rd",
    "2nd",
    "1st",
  ];

  return (
    <>
      <div className={styles.sidebar}>
        <ul>
          {items.map((item) => (
            <li key={item}>
              <Link href={`/subjects/${item}`}>
                <a>{item}</a>
              </Link>
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default Sidebar;
